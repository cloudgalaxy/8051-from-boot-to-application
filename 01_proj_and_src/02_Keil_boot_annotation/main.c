/*******************************************************************************
 * \brief	注释Keil自带的STARTUP.A51 boot汇编源码
 * \details	后面的工程会介绍如何自己用汇编写boot程序
 * \author	将狼才鲸
 * \date	2022-10-29
 ******************************************************************************/

/******************************** 接口函数 ************************************/
/**
 * \brief	什么都没做，只有死循环，这个工程主要是在注释汇编源文件
 */
int main()
{
	while(1);

	//return 0;
}

/********************************** 文件尾 ************************************/
