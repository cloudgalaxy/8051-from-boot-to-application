;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;File format: UTF-8
;本工程创建时间：2022-10-24

;R8051XC2 8DPTR CPU资源描述：
;flexible configurable, single-clock 8051 compatible IP core with 
;9.4 to 12.1 times more performance than legacy 80C51 
;(with Dhrystone v1.1 Benchmark on identical clock speed, 
;depending on the MDU, number of  DPTR and 
;auto inc/dec/switch implementation).

;Optional features: 
;32 I/O lines, 
;three 16-bit timer/counters, 
;compare/capture unit (CCU), 
;18 interrupts/4 priority levels or 6 interrupts/2 priority levels, 
;two serial interfaces (UARTs), 
;serial peripheral interface (SPI), 
;I2C interface, 
;secondary I2C interface, 
;16-bit multiplication-division unit (MDU), 
;multiple DPTR with auto-increment/auto-switch support, 
;15-bit programmable watchdog timer with configurable prescaler, 
;power management unit (PMU), 
;direct memory access (DMA) controller, 
;real time clock (RTC), 
;internally and externally generated wait states, 
;software reset, 
;program and data memory extension up to 8MB (banking). 

;Optionally available: On-Chip Debug Support for 
;Keil uVision Debugger.

;The R8051XC IP core can be implemented in FPGA and ASIC. 

;*** This device is configured for 8 DPTR registers ***
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;$NOPRINT	; 阻止编译器生成列表文件，已屏蔽

;================================ 头文件 =======================================
$INCLUDE (R8051XC2.INC)		; 文件包含，芯片SFR特殊功能寄存器定义
$INCLUDE (MACRO_FUNC.INC)	; 自定义的宏定义函数
$INCLUDE (USER_CONFIG.INC)	; 做配置用的宏定义
$INCLUDE (IRQ_ISR.ASM)		; 中断处理ISR，中断上半部
$INCLUDE (STARTUP.ASM)		; 用户执行的第一块汇编代码，你在这里面写程序
; 板子上电后第一条执行的0地址的代码就在这里面，中断处理函数的入口也在这里面
$INCLUDE (VECTOR_IRQ.ASM)	; 程序上电后第一个执行的复位，中断和异常的入口

; 主程序在上面STARTUP.ASM当中
