;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; R8051XC2芯片寄存器定义

; 寄存器字节定义
P0               DATA  80H
SP               DATA  81H
DPL              DATA  82H ; 数据指针DPTR
DPH              DATA  83H
WDTREL           DATA  86H
PCON             DATA  87H
TCON             DATA  88H
TMOD             DATA  89H
TL0              DATA  8AH
TL1              DATA  8BH
TH0              DATA  8CH
TH1              DATA  8DH
CKCON            DATA  8EH
P1               DATA  90H
DPS              DATA  92H
DPC              DATA  93H
PAGESEL          DATA  94H ; 大容量RAM分页
D_PAGESEL        DATA  95H ; 大容量RAM分页
S0CON            DATA  98H
S0BUF            DATA  99H
IEN2             DATA  9AH
S1CON            DATA  9BH
S1BUF            DATA  9CH
S1RELL           DATA  9DH
P2               DATA 0A0H
DMAS0            DATA 0A1H
DMAS1            DATA 0A2H
DMAS2            DATA 0A3H
DMAT0            DATA 0A4H
DMAT1            DATA 0A5H
DMAT2            DATA 0A6H
IEN0             DATA 0A8H
IP0              DATA 0A9H
S0RELL           DATA 0AAH
P3               DATA 0B0H
DMAC0            DATA 0B1H
DMAC1            DATA 0B2H
DMAC2            DATA 0B3H
DMASEL           DATA 0B4H
DMAM0            DATA 0B5H
DMAM1            DATA 0B6H
IEN1             DATA 0B8H
IP1              DATA 0B9H
S0RELH           DATA 0BAH
S1RELH           DATA 0BBH
IRCON2           DATA 0BFH
IRCON            DATA 0C0H
CCEN             DATA 0C1H
CCL1             DATA 0C2H
CCH1             DATA 0C3H
CCL2             DATA 0C4H
CCH2             DATA 0C5H
CCL3             DATA 0C6H
CCH3             DATA 0C7H
T2CON            DATA 0C8H
CRCL             DATA 0CAH
CRCH             DATA 0CBH
TL2              DATA 0CCH
TH2              DATA 0CDH
RTCSEL           DATA 0CEH
RTCDAT           DATA 0CFH
PSW              DATA 0D0H ; 程序状态字
IEN4             DATA 0D1H
I2C2DAT          DATA 0D2H
I2C2ADR          DATA 0D3H
I2C2CON          DATA 0D4H
I2C2STA          DATA 0D5H
SMB2_SEL         DATA 0D6H
SMB2_DST         DATA 0D7H
ADCCON           DATA 0D8H
I2CDAT           DATA 0DAH
I2CADR           DATA 0DBH
I2CCON           DATA 0DCH
I2CSTA           DATA 0DDH
SMB_SEL          DATA 0DEH
SMB_DST          DATA 0DFH
ACC              DATA 0E0H ; 累加器，使用频率最高的寄存器
SPSTA            DATA 0E1H
SPCON            DATA 0E2H
SPDAT            DATA 0E3H
SPSSN            DATA 0E4H
MD0              DATA 0E9H
MD1              DATA 0EAH
MD2              DATA 0EBH
MD3              DATA 0ECH
MD4              DATA 0EDH
MD5              DATA 0EEH
ARCON            DATA 0EFH
B                DATA 0F0H ; 寄存器B，可做通用寄存器，在乘除法中与A寄存器配合
SRST             DATA 0F7H

; 寄存器位定义，地址位数是0和8的能位寻址
CY               BIT  PSW.7	; 加减法进位
AC               BIT  PSW.6 ; 辅助进位
F0               BIT  PSW.5 ; 用户标志，可自行使用
RS1              BIT  PSW.4 ; r0~r7寄存器从4组中选择一组
RS0              BIT  PSW.3
OV               BIT  PSW.2 ; 加减法溢出
F1               BIT  PSW.1 ; 保留位
P                BIT  PSW.0 ; 累加器中奇偶校验

TF1              BIT  TCON.7
TR1              BIT  TCON.6
TF0              BIT  TCON.5
TR0              BIT  TCON.4
IE1              BIT  TCON.3
IT1              BIT  TCON.2
IE0              BIT  TCON.1
IT0              BIT  TCON.0

T2PS             BIT  T2CON.7
I3FR             BIT  T2CON.6
I2FR             BIT  T2CON.5
T2R1             BIT  T2CON.4
T2R0             BIT  T2CON.3
T2CM             BIT  T2CON.2
T2I1             BIT  T2CON.1
T2I0             BIT  T2CON.0

SM               BIT  S0CON.7
SM1              BIT  S0CON.6
SM2              BIT  S0CON.5
REN              BIT  S0CON.4
TB8              BIT  S0CON.3
RB8              BIT  S0CON.2
TI               BIT  S0CON.1
RI               BIT  S0CON.0

EAL              BIT  IEN0.7 ; 所有中断使能
WDT              BIT  IEN0.6
ET2              BIT  IEN0.5
ES0              BIT  IEN0.4
ET1              BIT  IEN0.3
EX1              BIT  IEN0.2
ET0              BIT  IEN0.1
EX0              BIT  IEN0.0

EXEN2            BIT  IEN1.7
SWDT             BIT  IEN1.6
EX6              BIT  IEN1.5
EX5              BIT  IEN1.4
EX4              BIT  IEN1.3
EX3              BIT  IEN1.2
EX2              BIT  IEN1.1
EX7              BIT  IEN1.0

EXF2             BIT  IRCON.7
TF2              BIT  IRCON.6
IEX6             BIT  IRCON.5
IEX5             BIT  IRCON.4
IEX4             BIT  IRCON.3
IEX3             BIT  IRCON.2
IEX2             BIT  IRCON.1
IEX7             BIT  IRCON.0

BD               BIT  ADCCON.7
