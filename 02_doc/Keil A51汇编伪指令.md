# Keil A51汇编伪指令

| 作者   | 将狼才鲸       |
| ---- | ---------- |
| 创建日期 | 2022-09-29 |

---

## Keil伪指令

| 伪指令                | 含义                               | 举例                                     |
| ------------------ | -------------------------------- | -------------------------------------- |
| $NOMOD51           | Ax51宏汇编器控制命令：禁止预定义的8051          | STARTUP.A51第一行第一句话                     |
| EQU                | 进行宏定义                            | IDATALEN        EQU     80H            |
| #xx                | 对宏定义进行取值                         | #IDATALEN                              |
| xxH                | 数字开头，H结尾的是16进制数，如果数字是A-F开头，则前面补0 | 80H                                    |
| IF <>              | 如果某某宏定义不等于                       | IF IDATALEN <> 0                       |
| ENDIF              | 和IF配套使用                          |                                        |
| #LOW (xx)          | 16位宏定义数的低字节                      | #LOW (XDATALEN)                        |
| #HIGH (xx）         | 16位宏定义数的高字节                      | #HIGH (XDATALEN)                       |
| EXTRN              | 定义外部声明的符号                        | EXTRN DATA (?C_IBP)                    |
| EXTERN CODE()      | 声明外部函数，C语言函数                     | EXTERN CODE(_Main)                     |
| DATA               | 定义数据，类似于宏定义                      | ACC DATA 0E0H                          |
| ()                 | 括号控制优先级                          | EXTRN DATA (?C_IBP)                    |
| ?C_IBP             | 问号开头是Keil内部函数或地址指针               | EXTRN DATA (?C_IBP)                    |
| NAME               | 声明本文件模块函数入口                      | NAME ?C_STARTUP                        |
| RSEG               | 用再定位段做当前段                        | RSEG ?C_C51STARTUP                     |
| SEGMENT            | 声明段名, 如代码段                       | ?STACK SEGMENT IDATA                   |
| DS                 | 从当前位置预留N字节备用                     | DS 1                                   |
| CODE               | 指示指针内容属于代码段                      | EXTRN CODE (?C_START)                  |
| PUBLIC             | 输出本地的函数标量                        | PUBLIC  ?C_STARTUP                     |
| CSEG               | 绝对代码段声明                          | CSEG AT 0                              |
| AT                 | 段地址                              | CSEG AT 0                              |
| #if                | 条件编译                             | #if 0                                  |
| #endif             | 条件编译                             |                                        |
| END                | 汇编文件结束                           |                                        |
| $SAVE              | 存储最近的LIST和GEN的设置                 |                                        |
| $NOLIST            | 不使用最近的LIST配置                     |                                        |
| $RESTORE           | 恢复最近的LIST和GEN的设置                 |                                        |
| $INCLUDE           | 文件包含                             | $INCLUDE (REG8051.INC)                 |
| XXX MACRO XXX ENDM | 宏定义函数                            | PUSH_ACC MACRO '换行' PUSH ACC '换行' ENDM |
| ORG                | 起始伪指令, 从哪个地址开始读取并执行代码            | ORG 00H                                |
| DB                 | 写入数据                             | DB 00H, 00H                            |

* *参考资料*
  [8051单片机基础4：数据存储类型（data,idata,xdata,code）](https://blog.csdn.net/u010160335/article/details/109416191)
  [CX51 用户手册----SAVE伪指令](https://blog.csdn.net/whb_mcu/article/details/7774446)