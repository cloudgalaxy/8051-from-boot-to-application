* 本工程及源码内容介绍：

```shell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 介绍：	【51单片机汇编】用Keil模拟器查看IO口输出
; NOTE:		File format: UTF-8
; 备注：	1、复位和中断运行起始地址介绍详见STC官方文档《STC89C52系列单片机器件手册》
;			第6章 中断系统；
;			2、CSEG等所有Keil汇编伪指令的含义详见Keil安装目录下的文档
;			Keil_v5\C51\Hlp\a51.chm，文档里支持搜索；
;			3、8051 CPU内核的寄存器含义的介绍可参考大学的单片机原理等课程，也可以看
;			STC官方文档《STC89C52系列单片机器件手册》;只想看原版8051指令集文档，
;			不想看教材的话，可以搜索下载“Atmel 8051 Microcontrollers Hardware Manual”，
;			或者“MCS ® 51 Family of Microcontrollers Architectural Overview”；
;			本Git仓库doc文件夹内有上述文档的下载链接；
;			4、本工程默认使用Keil模拟器运行，无需硬件，按F7编译后，按Ctrl+F5开始
;			运行，在Keil软件的Logic Analyzer模拟示波器窗口中看IO口输出结果；
; 查看方法：
;			1、本工程中已配置好窗口，但软件刚打开时波形太密不好观察，需要将鼠标
;			放在Logic Analyzer窗口的P0波形上，按住电脑键盘的Ctrl键，然后向前滚动
;			鼠标滚轮，放大波形后即可看到P0.0IO口输出的矩形波
;			2、如果你是自己创建的工程，则先按Ctrl+F5开始运行后，点击Keil软件上方菜单
;			的View-->Analysis Windows-->Logic Analyzer（如果是打开本仓库已下载的工程，
;			点一下则会让该示波器窗口消失，再按上述操作一遍则会再出现），
;			点击Logic Analyzer窗口的Setup，在弹出窗口中点击右边X叉叉左边的图标New(Insert)
;			-->在出来的输入栏里输入P0.0并回车，然后鼠标在刚刚出现的P0这一行上点一下，
;			然后在Max:文本输入框内将0xFF改成1，然后点击该弹出窗口的Close，
;			然后按F5运行程序，然后点击Stop按钮停止运行，则能在Keil虚拟示波器
;			的P0栏看到输出的矩形波；
; 作者		将狼才鲸
; 日期		2023-06-10
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
```

* 51单片机使用Keil模拟器查看IO输出的效果：

![img](./Keil_C51模拟器IO口输出实际效果.JPG)