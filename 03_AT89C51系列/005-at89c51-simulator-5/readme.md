* 本工程及源码内容介绍：

```shell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 介绍：	【51单片机汇编】用Keil C51模拟器的UART#1窗口查看串口输出
; NOTE:		File format: UTF-8
; 备注：	1、本工程默认使用Keil模拟器运行，无需硬件；按F7编译后，按Ctrl+F5运行，
;			调出UART #1窗口（要先运行程序）：点击软件的View-->Serial Windows-->UART #1，
;			在Keil软件的UART #1窗口中看串口的输出结果为Hello world!....；
;			2、使用C语言从Keil模拟器输出串口信息的步骤详见我的另外两篇介绍：
;			https://gitee.com/langcai1943/8051-from-boot-to-application#1hello-world%E8%BE%93%E5%87%BA
;			https://gitee.com/langcai1943/8051-from-boot-to-application#5%E7%94%A8%E6%B1%87%E7%BC%96%E4%BB%8Ekeil%E8%B0%83%E8%AF%95%E7%AA%97%E5%8F%A3%E4%B8%AD%E8%BE%93%E5%87%BAhello-world
; 作者		将狼才鲸
; 日期		2023-06-11
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
```

* 可能遇到的问题
1. 如果是你自己创建的工程，使用Keil C51模拟器，运行程序时发现Keil的Command窗口持续报错：  
error 65: access violation at   I:0xC2 : no 'write' permission  
则可能时用Keil创建的AT89C51工程默认不会设置外部RAM，而内部RAM只有0x80的大小，设置堆栈MOV SP, #0C0H的话，则程序越界了，可以MOV SP, #3FH  

2. 如果你是第一次写8051汇编程序，写了一个汇编函数，然后在复位中断里用LCALL/ACALL/CALL等命令调用了它，然后单步执行，但是按F10时发现怎么也进不去子函数。那不是你程序写错了，而是Keil C51的单步执行和C语言的一样，遇到LCALL它就知道是一个函数，就自己跳过去了，此时你需要在子函数设置一个断点，然后按F5直接运行并进入断点，或者F10单步执行时在CALL DELAY这里改按F11，进入子函数，这样才能继续单步进子函数。我找这个问题找了几个小时才发现是这样的，在网上搜完全没搜到结果。  

例如：如果用汇编写了一个子函数

```shell
	ORG 0H
	LJMP RESET
	
	ORG 23H
	RETI

RESET:
LOOP:
	CALL DELAY
	LJMP LOOP
	
DELAY:
	NOP
	RET

END
```



