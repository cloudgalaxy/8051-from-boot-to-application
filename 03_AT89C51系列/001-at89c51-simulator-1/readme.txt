/******************************************************************************
 * \brief	使用Keil创建一个AT89C51芯片的默认工程
 * \details	创建默认工程时，Keil会自行创建一个STARTUP.A51的boot文件，该文件中
 *			会直接跳转到C语言的main函数，但我后续不会用这个默认文件，而是自行
 *			从头开始写起
 * \remarks	AT89C51的芯片资料，可以看STC的文档，寄存器是一样的，
 *			STC89C52系列单片机器件手册 http://www.stcmcudata.com/datasheet/STC89C52.pdf
 *			文档里没有8051的通用寄存器介绍，8051的基础知识需要找本51单片机的书进行了解
 * \note	File format: UTF-8
 * \author	将狼才鲸
 * \date	2023-06-08
 ******************************************************************************/
