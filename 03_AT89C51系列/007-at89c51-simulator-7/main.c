/* C源码里什么内容都没有，代码都在汇编里 */

#include <stdio.h>

//TODO: 增加定时器的准确配置
//	只需掌握定时器0的模式3，不可屏蔽中断16位自动装在
//TODO: 增加串口的准确配置
//TODO：增加一个时钟应用

/**
 * \brief	C语言主函数
 * \param	argc:	参数数目
 * \param	argv:	参数数组
 * \return	错误码
 * \note	注意：1、如果是直接用Keil创建的默认工程，未在Project-->Options...
 *					-->Debug界面的左侧取消勾选Run to main()的话，按F5运行后程序会
 *					直接进入main函数第一行，可以在BOOT.ASM中的MAIN_JUMP函数位置，
 *					或者START.ASM中的LJMP RESET处先打个断点，然后在main函数前面进入
 *					断点；也可以像上面在Options配置中取消勾选Run to main()来直接进入
 *					0地址的复位中断；
 */
int main(int argc, void *argv[])
{
	int i = 0;
	char array[] = {0x01, 0x02, 0x03};
	
	//printf("NULL\n");	/* 此时输出还没有重定向，printf无效，而且会在里面阻塞住 */

	/* 进入死循环，阻塞住，让程序执行完后不退出 */
	while (1) {
		i++;	/* 什么也没做，便于在此打个断点 */
	};
}
