* 本工程及源码内容介绍：

```shell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 介绍：	【51单片机汇编】用Keil C51模拟器对IO口输入，并产生中断，在模拟器输出窗口用串口打印出来
; NOTE:		File format: UTF-8
; 备注：	1、本工程默认使用Keil模拟器运行，无需硬件；按F7编译后，按Ctrl+F5运行，
;			调出UART #1窗口（要先运行程序）：点击软件的View-->Serial Windows-->UART #1，
;			后续要在Keil软件的UART #1窗口中看串口的输出结果；
;			2、点击软件Peripherals-->IO-Ports-->Port 0，调出IO0输入的窗口，第一行选中
;			bit1，在第二行bit1的位置勾选就是输入1，取消勾选就是输入0；此时如果勾选了，
;			下面UART #1窗口就会不断输出Hello world，取消勾选了就停止输出；
; 作者		将狼才鲸
; 日期		2023-06-11
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
```

* Keil模拟器运行的效果：
![img](./8051获取到Keil模拟器IO输入信号.JPG)

* 遇到的问题1：
* 如果你自建的汇编原文件不是以Keil默认的.A51或者.s .S结尾，而是以.asm .ASM结尾的话，在Keil工程里包含新文件时，不要使用文件类型：All files(*.*)，而要使用文件类型：Asm Source file(*.s; *.src; *.a*)，否则Keil编译会报错，无法创建中间文件的路径。

* 遇到的问题2：
* 不使用Keil的默认文件，如何自己写Boot汇编文件并跳转到Main函数？自己写的代码编译时会报错
* Keil C51从汇编跳转到main函数编译器报错ERROR L127: UNRESOLVED EXTERNAL SYMBOL的解决办法
* 报错信息如下：

```shell
; Keil工程里面就新建2个文件，而且不使用Keil的默认Boot汇编文件，并自动跳转到main函数的功能
; STARTUP.A51:
EXTERN CODE (_MAIN)

	ORG 00H
	LJMP _MAIN
	RETI

;	ORG 0100H
;RESET:
;	NOP 
;	RET

	END

// main.c
int main()
{
	return 0;
}
```

* 发现有编译报错：

```
Rebuild started: Project: aaa
Rebuild target 'Target 1'
assembling STARTUP.A51...
compiling main.c...
linking...
*** WARNING L30: MEMORY SPACE OVERLAP
    SEG:     ?CO?STARTUP?0
    FROM:    C:000000H   
    TO:      C:000003H   
*** ERROR L127: UNRESOLVED EXTERNAL SYMBOL
    SYMBOL:  _MAIN
    MODULE:  .\Objects\STARTUP.obj (STARTUP)
*** ERROR L128: REFERENCE MADE TO UNRESOLVED EXTERNAL
    SYMBOL:  _MAIN
    MODULE:  .\Objects\STARTUP.obj (STARTUP)
    ADDRESS: 1000001H
Program Size: data=9.0 xdata=0 const=0 code=23
Target not created.
Build Time Elapsed:  00:00:01
```

* 解决办法：
* 这是因为你不能像Keil默认工程那样使用int main()这种定义，这里面有返回值，必须要用void main(int param)才行，即使使用void main()也不行，也会报错；
