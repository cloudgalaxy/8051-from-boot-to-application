/* C源码里什么内容都没有，代码都在汇编里 */

#include <stdio.h>

/**
 * \brief	C语言主函数
 * \param	param:	未使用
 * \return	无
 * \note	注意：1、自己写的汇编跳转到main函数，不能用int main(), void main()的写法，
 *					必须要用void main(int)，否则编译器会报错，并且给不出有效提示！
 *				  2、如果是直接用Keil创建的默认工程，未在Project-->Options...
 *					-->Debug界面的左侧取消勾选Run to main()的话，按F5运行后程序会
 *					直接进入main函数第一行，可以在BOOT.ASM中的MAIN_JUMP函数位置，
 *					或者START.ASM中的LJMP RESET处先打个断点，然后在main函数前面进入
 *					断点；也可以像上面在Options配置中取消勾选Run to main()来直接进入
 *					0地址的复位中断；
 *				  3、汇编文件名不要取成MAIN.ASM，这会和Keil的一些预定义段符号冲突而
 *					报错*** WARNING L7: MODULE NAME NOT UNIQUE
 */
void main(int param)
{
	int i = 0;
	
	//printf("NULL\n");	/* 此时输出还没有重定向，printf无效，而且会在里面阻塞住 */

	/* 进入死循环，阻塞住，让程序执行完后不退出 */
	while (1) {
		i++;	/* 什么也没做，便于在此打个断点 */
	};
}
