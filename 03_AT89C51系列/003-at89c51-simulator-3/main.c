/******************************************************************************
 * \brief	使用汇编从芯片上电的起始位置开始执行代码，演示各个中断的入口
 * \details	不使用Keil自行定义的内部符号，这样直接跳转到C语言的main函数，看不到
 *			芯片的具体执行和上电过程
 * \remarks	想直接运行和学习一些8051工程和源码的话，可以浏览Keil安装目录的
 *			Keil_v5\C51\Examples，里面有一些官方Demo；
 *			想看各种文档的话，目录在D:\Keil_v5\C51\Hlp
 * \note	File format: UTF-8
 * \author	将狼才鲸
 * \date	2023-06-08
 ******************************************************************************/

int main()
{
	// 内容为空，从汇编开始执行
	return 0;
}
